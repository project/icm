<?php

namespace Drupal\icm\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\Core\Field\FormatterBase;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\paragraphs\ParagraphInterface;

/**
 * Plugin implementation of "icm_field_formatter" formatter.
 *
 * @FieldFormatter(
 *   id = "icm_field_formatter",
 *   label = @Translation("Image Compare Viewer"),
 *   field_types = {
 *     "image",
 *     "entity_reference"
 *   }
 * )
 */
class IcmFieldFormatter extends FormatterBase {
  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The image style entity storage.
   *
   * @var \Drupal\image\ImageStyleStorageInterface
   */
  protected $imageStyleStorage;

  /**
   * Constructs an ImageFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *   Thrown if the entity type doesn't exist.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   *   Thrown if the storage handler couldn't be loaded.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, AccountInterface $current_user, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->currentUser = $current_user;
    $this->entityTypeManager = $entity_type_manager;
    $this->imageStyleStorage = $this->entityTypeManager->getStorage("image_style");
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
          $plugin_id,
          $plugin_definition,
          $configuration["field_definition"],
          $configuration["settings"],
          $configuration["label"],
          $configuration["view_mode"],
          $configuration["third_party_settings"],
          $container->get("current_user"),
          $container->get("entity_type.manager"),
      );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      "image_style" => "",
      "effects" => "",
      "icm_link_image_to" => "",
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = [];

    $image_styles = image_style_options(FALSE);

    $description_link = Link::fromTextAndUrl(
          $this->t("Configure Image Styles"),
          Url::fromRoute("entity.image_style.collection")
      );

    $element["image_style"] = [
      "#title" => $this->t("Image style"),
      "#type" => "select",
      "#default_value" => $this->getSetting("image_style"),
      "#empty_option" => $this->t("None (original image)"),
      "#options" => $image_styles,
      "#description" => $description_link->toRenderable() + [
        "#access" => $this->currentUser->hasPermission("administer image styles"),
      ],
    ];

    $mode = [
      "horizontal" => $this->t("Horizontal"),
      "vertical" => $this->t("Vertical"),
      "45deg" => $this->t("45deg"),
    ];

    $element["effects"] = [
      "#title" => $this->t("style effects"),
      "#type" => "select",
      "#default_value" => $this->getSetting("effects"),
      "#options" => $mode,
      "#description" => $this->t("Should the image slider be vertical or horizontal?"),
    ];

    $link_image_to = ["" => "Nothing", "content" => "Content", "file" => "File"];

    $element["icm_link_image_to"] = [
      "#type" => "select",
      "#title" => $this->t("Link image to"),
      "#options" => $link_image_to,
      "#default_value" => $this->getSetting("icm_link_image_to"),
    ];
    return $element;

  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $image_styles = image_style_options(FALSE);
    unset($image_styles[""]);
    $image_style_setting = $this->getSetting("image_style");
    if (isset($image_styles[$image_style_setting])) {
      $summary[] = $this->t("Image style: @style", ["@style" => $image_styles[$image_style_setting]]);
    }
    else {
      $summary[] = $this->t("Original image");
    }

    $effects = $this->getSetting("effects");
    if (isset($effects)) {
      $summary[] = $this->t("Effect: @vertical_mode_style_effect", [
        "@vertical_mode_style_effect" => $effects,
      ]);
    }

    $link_image_to = $this->getSetting("icm_link_image_to");
    if ($link_image_to) {
      $summary[] = $this->t("Link Image to: @link_image_to", [
        "@link_image_to" => $link_image_to,
      ]);
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $image_style = NULL;

    $image_style_setting = $this->getSetting("image_style");

    if (!empty($image_style_setting)) {
      $image_style = $this->entityTypeManager->getStorage("image_style")->load($image_style_setting);
    }
    $image_url_values = [];
    foreach ($items as $item) {
      // Handle image fields directly.
      if ($item->getFieldDefinition()->getType() === 'image') {
        $file = $this->entityTypeManager->getStorage('file')->load($item->target_id);
        if ($file) {
          $image_uri = $file->getFileUri();
          if ($image_style) {
            $image_uri = $image_style->buildUrl($image_uri);
          }
          else {
            $image_uri = $file->createFileUrl(FALSE);
          }
          $image_url_values[] = ["url" => $image_uri];
        }
        // Handle media fields referencing image media entities.
      }
      elseif ($item->entity && $item->entity->bundle() === 'image') {
        $media = $item->entity;
        $file = $media->get('field_media_image')->entity;
        if ($file) {
          $image_uri = $file->getFileUri();
          if ($image_style) {
            $image_uri = $image_style->buildUrl($image_uri);
          }
          else {
            $image_uri = $file->createFileUrl(FALSE);
          }
          $image_url_values[] = ["url" => $image_uri];
        }
      }
    }

    // Populate the title and alt of images based on fid.
    $field_name = $this->fieldDefinition->getName();

    // Get the Entity value as array.
    $entity = $items->getEntity();

    // Get style horizontal/Vertical for the images image.
    $effects = $this->getSetting("effects");
    $icm_link_image_to = $this->getSetting("icm_link_image_to");
    $link_image_to = [];

    if ($entity instanceof NodeInterface || $entity instanceof ParagraphInterface) {

      $arrayNode = $entity->toArray();
      if (array_key_exists($field_name, $arrayNode)) {
        foreach ($arrayNode[$field_name] as $key => $value) {
          $image_url_values[$key]["alt"] = (isset($value["alt"])) ? $value["alt"] : "";
          $image_url_values[$key]["title"] = (isset($value["title"])) ? $value["title"] : "";
        }
      }

      $link_image_to["type"] = $icm_link_image_to;
      if ($icm_link_image_to == "content") {
        $link_image_to["path"] = Url::fromRoute("entity.node.canonical", ["node" => $entity->id()]);
      }
      elseif ($icm_link_image_to == "") {
        $link_image_to = FALSE;
      }
    }

    if (!empty($image_url_values)) {
      $elements[] = [
        "#theme" => "icm",
        "#url" => $image_url_values,
        "#effect" => $effects,
        "#link_image_to" => $link_image_to,
      ];

      // Attach the image field slide show library.
      $elements["#attached"]["library"][] = "icm/icm";
    }

    return $elements;

  }

  /**
   * Check if used plugin is  icm_field_formatter.
   */
  protected function isIcmDisplay() {
    return $this->getPluginId() == "icm_field_formatter";
  }

}
