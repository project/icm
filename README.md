# Image Compare Viewer

## CONTENTS OF THIS FILE

 - Introduction
 - Requirements
 - Installation
 - Configuration
 - Maintainers

## INTRODUCTION

This will provide a field formatter for image fields,
so that the images uploaded for an image field can be 
rendered as Image Compare Viewer You can add 2 or more images .

## REQUIREMENTS

This module dependency on jquery_ui <https://www.drupal.org/project/jquery_ui>


## INSTALLATION

- Download the module and place in contrib module folder.
- Enable the icm module from the modules page / drush / drupal console.
- You should now see a new field formatter for image fields,
  Ex: under Manage display section of each content types.

## CONFIGURATION

- Visit any image fields display settings, you will be able to find
the Image Compare Viewer formatter, select this one and one can also
select image styles.
Ex: admin/structure/types/manage/<content-type-machine-name>/display
- Have the image field settings "Allowed number of values"
to unlimited / limited(more than 1 image).
- Have a custom image style defined under
<http://d10.local/admin/config/media/image-styles>
- Add content & upload more than 1 images to the node and Save..
On node view, Image Compare Viewer will appear for that image field.


## MAINTAINERS

 - Ghazali Mustapha (g.mustapha) - <https://www.drupal.org/u/gmustapha>
