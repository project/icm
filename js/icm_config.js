(function ($, Drupal, drupalSettings) {
    $(document).ready(function () {
      let icm = document.querySelectorAll('.icm')
      let icmLength = icm.length;
      for (let i = 0; i < icmLength; i++) {
        if(hasClass(icm[i], "icm-vertical") ){
          new Dics({
            container: icm[i],
            hideTexts: true,
            linesOrientation: 'vertical',
          });
        }
        if(hasClass(icm[i], "icm-horizontal") ){
          new Dics({
            container: icm[i],
            hideTexts: true,
            linesOrientation: 'horizontal',
          });
        }
        if(hasClass(icm[i], "icm-45deg") ){
          new Dics({
            container: icm[i],
            hideTexts: true,
            rotate: '45deg'
          });
        }
      }
      function hasClass(element, className) {
        return (' ' + element.className + ' ').indexOf(' ' + className+ ' ') > -1;
      }
    });
})(jQuery, Drupal, drupalSettings);